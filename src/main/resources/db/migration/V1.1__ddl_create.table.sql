
-- DROP DATABASE IF EXISTS `yq_business`;
-- CREATE DATABASE `yq_business` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `cms`;

--  -- ----------------------------
-- Table structure for rg_admin_user
-- --------------------------------
DROP TABLE IF EXISTS `rg_admin_user`;
CREATE TABLE `rg_admin_user`(
`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
`username` VARCHAR(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT'用户名（相当于微信号）',
`nick_name` VARCHAR(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT'昵称（相当于微信网名）',
`tel` VARCHAR(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT'电话',
`email` VARCHAR(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT'邮箱',
`password` VARCHAR(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT'密码',
`salt` VARCHAR(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT'加密盐值',
`status` TINYINT(4) DEFAULT '1' NOT NULL COMMENT '账号状态 0禁用 1正常',
`is_delete` TINYINT(4) DEFAULT '0' NOT NULL  COMMENT '是否已删除 0未 1删',
`last_login_time` DATETIME DEFAULT NULL COMMENT '最近一次登录时间',
`created_at` DATETIME DEFAULT NULL COMMENT '创建时间',
`updated_at` DATETIME DEFAULT NULL COMMENT '更新时间',
PRIMARY KEY (`id`)
)ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='后端用户表';

-- ----------------------------
-- Table structure for rg_role
-- ----------------------------
DROP TABLE IF EXISTS `rg_role`;
CREATE TABLE `rg_role`(
`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
`name` VARCHAR(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT'角色名称',
`desc` VARCHAR(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT'角色描述',
`operator_num` INT(10) DEFAULT '0' NOT NULL COMMENT '操作员数量',
`status` TINYINT(4) DEFAULT '1' NOT NULL COMMENT '角色状态 0禁用 1正常',
`created_by` BIGINT(20) UNSIGNED NOT NULL DEFAULT '1' COMMENT'创建人id，默认值为超级管理员',
`created_at` DATETIME DEFAULT NULL COMMENT '创建时间',
`updated_at` DATETIME DEFAULT NULL COMMENT '更新时间',
PRIMARY KEY (`id`)
)ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='角色表';

-- ---------------------------------
-- Table structure for rg_menu
-- ---------------------------------
DROP TABLE IF EXISTS `rg_menu`;
CREATE TABLE `rg_menu`(
`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
`name` VARCHAR(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT'菜单名称',
`path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '路由地址',
`component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '组件路径(路由)',
`visible` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '菜单状态（0隐藏 1显示）',
`status` TINYINT(4) NOT NULL  DEFAULT '1' COMMENT '菜单状态（0停用 1正常）',
`permission` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '权限标识（权限编码）',
`icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#' COMMENT '菜单图标',
`pid` BIGINT(20) UNSIGNED NOT NULL DEFAULT '0' COMMENT'父级id',
`is_delete` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '是否删除（0未删除 1已删除）',
`created_at` DATETIME DEFAULT NULL COMMENT '创建时间',
`updated_at` DATETIME DEFAULT NULL COMMENT '更新时间',
PRIMARY KEY (`id`)
)ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='菜单表';

-- ---------------------------------------------
-- Table structure for rg_user_role_mid
-- ---------------------------------------------
DROP TABLE IF EXISTS `rg_user_role_mid`;
CREATE TABLE `rg_user_role_mid`(
`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
`user_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT '0' COMMENT'用户id',
`role_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT '0' COMMENT'权限id',
PRIMARY KEY (`id`)
)ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户角色中间表';

-- ---------------------------------------------
-- Table structure for rg_role_menu_mid
-- ---------------------------------------------
DROP TABLE IF EXISTS `rg_role_menu_mid`;
CREATE TABLE `rg_role_menu_mid`(
`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
`role_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT '0' COMMENT'角色id',
`menu_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT '0' COMMENT'权限id',
PRIMARY KEY (`id`)
)ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='角色菜单中间表';

-- ---------------------------------------------
-- Table structure for rg_core_attach
-- ---------------------------------------------
DROP TABLE IF EXISTS `rg_core_attach`;
CREATE TABLE `rg_core_attach`(
`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
`file_name` VARCHAR(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT'文件名称',
`file_path` VARCHAR(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT'文件路径',
`created_at` DATETIME DEFAULT NULL,
`updated_at` DATETIME DEFAULT NULL,
PRIMARY KEY (`id`)
)ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='文件上传表';