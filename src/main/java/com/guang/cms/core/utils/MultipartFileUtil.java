package com.guang.cms.core.utils;


import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;

public class MultipartFileUtil {
    public static final int BUF_SIZE = 1024 * 1024;

    /**
     * @Author lrg
     * @Description 把multipartFile流文件写入到本地url中
     * @Date 17:46 2023/4/27
     * @Param [file, url, filename]文件流，本地路径
     * @return void
     **/
    public static void addFile(MultipartFile file, String url, String filename){
        OutputStream outputStream = null;
        InputStream inputStream = null;
        try {
            inputStream = file.getInputStream();
            //log.info("fileName="+fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            // 2、保存到临时文件
            // 1K的数据缓冲
            byte[] bs = new byte[1024];
            // 读取到的数据长度
            int len;
            // 输出的文件流保存到本地文件
            File tempFile = new File(url);
            if (!tempFile.exists()) {
                tempFile.mkdirs();
            }

            //跨平台写法，windows和linux都适用
            outputStream = new FileOutputStream(tempFile.getPath()+"\\"+filename);

            // 开始读取和写入os
            while ((len = inputStream.read(bs)) != -1) {
                outputStream.write(bs, 0, len);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 完毕，关闭所有链接
            try {
                outputStream.close();
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @Author lrg
     * @Description 通过随机访问IO即RandomAccessFile合并某文件夹里面的所有文件为一个文件
     * @Date 17:46 2023/4/27
     * @Param [fromUrl, filename]文件夹里装有所有的分片，新的文件名
     * @return void
     **/
    public static void mergeFileByRandomAccessFile(String fromUrl, String filename) {
        String toUrl = fromUrl.substring(0, fromUrl.lastIndexOf("\\")+1)+"\\"+filename;

        RandomAccessFile in = null;
        RandomAccessFile out = null;
        System.out.println(fromUrl);
        File[] files = new File(fromUrl).listFiles();
        //必须保证有序，名字根据编号排。避免默认的字典序，即1后面是10编号而不是2
        Arrays.sort(files, (o1, o2) -> Integer.parseInt(o1.getName())-Integer.parseInt(o2.getName()));
        for(File f : files) {
            System.out.println(f.getPath());
        }
        try {
            out = new RandomAccessFile(toUrl, "rw");
            for (File file : files) {
                in = new RandomAccessFile(file, "r");
                int len = 0;
                byte[] bt = new byte[BUF_SIZE];
                while (-1 != (len = in.read(bt))) {
                    out.write(bt, 0, len);
                }
                in.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @Author lrg
     * @Description 通过流式NIO即FileChannel合并某文件夹里面的所有文件为一个文件
     * @Date 17:46 2023/4/27
     * @Param [fromUrl, filename]文件夹里装有所有的分片，新的文件名
     * @return void
     **/
    public static void mergeFileByFileChannel(String fromUrl, String filename) {
        String toUrl = fromUrl.substring(0, fromUrl.lastIndexOf("\\")+1)+"\\"+filename;
        FileChannel outChannel = null;
        FileChannel inChannel = null;
        File[] files = new File(fromUrl).listFiles();
        try {
            outChannel = new FileOutputStream(toUrl).getChannel();
            for (File file : files) {
                inChannel = new FileInputStream(file).getChannel();
                ByteBuffer bb = ByteBuffer.allocate(BUF_SIZE);
                while (inChannel.read(bb) != -1) {
                    bb.flip();
                    outChannel.write(bb);
                    bb.clear();
                }
                inChannel.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (outChannel != null) {
                    outChannel.close();
                }
                if (inChannel != null) {
                    inChannel.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @Author lrg
     * @Description 递归删除一个含有文件或者子文件夹的文件夹
     * @Date 17:45 2023/4/27
     * @Param [url]文件夹地址
     * @return void
     **/
    public static void deleteDirByNio(String url) throws Exception {
        Path path = Paths.get(url);
        Files.walkFileTree(path,
                new SimpleFileVisitor<Path>() {
                    // 先去遍历删除文件
                    @Override
                    public FileVisitResult visitFile(Path file,
                                                     BasicFileAttributes attrs) throws IOException {
                        Files.delete(file);
                        return FileVisitResult.CONTINUE;
                    }
                    // 再去遍历删除目录
                    @Override
                    public FileVisitResult postVisitDirectory(Path dir,
                                                              IOException exc) throws IOException {
                        Files.delete(dir);
                        return FileVisitResult.CONTINUE;
                    }
                }
        );
    }
}


