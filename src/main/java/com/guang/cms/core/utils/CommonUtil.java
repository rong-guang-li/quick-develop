package com.guang.cms.core.utils;

import java.util.*;

public class CommonUtil {

    /**
     * UUID随机生成字符串
     *
     * @return
     */
    public static String getUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    /**
     * 判断collection
     * @param list
     * @return不为null且不为空则true
     */
    public static Boolean listNotEmpty(List list){

        if(list==null||list.isEmpty()){
            return false;
        }
        return true;
    }
}




