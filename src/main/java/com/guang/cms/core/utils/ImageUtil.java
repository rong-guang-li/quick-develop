package com.guang.cms.core.utils;

import org.springframework.util.StringUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;

public class ImageUtil {

    /**
     * @Author lrg
     * @Description 图片转Base64编码
     * @Date 14:15 2023/4/24
     * @Param [bufferedImage]
     * @return java.lang.String
     **/
    public static String toBase64(BufferedImage bufferedImage){
        Base64.Encoder encoder = Base64.getEncoder();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            String res = "data:image/jpg;base64,";
            ImageIO.write(bufferedImage, "jpg", outputStream);
            String base64Img = encoder.encodeToString(outputStream.toByteArray());
            if(StringUtils.isEmpty(base64Img)){
                return "";
            }
            res = res + base64Img;
            return res;
        } catch (IOException e) {
            return "";
        }finally {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
