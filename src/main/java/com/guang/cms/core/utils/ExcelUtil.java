package com.guang.cms.core.utils;



import com.guang.cms.domain.dto.common.excel.ExcelDataDto;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ExcelUtil {
    public static void exportExcel(HttpServletResponse response, String fileName, ExcelDataDto data) throws Exception {
        // 告诉浏览器用什么软件可以打开此文件
        response.setHeader("content-Type", "application/vnd.ms-excel");
        // 下载文件的默认名称
        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "utf-8"));
        exportExcel(data, response.getOutputStream());
    }

    /**
     * @Author lrg
     * @Description 获取输入流的数据：获取上传的excel文件的数据
     * @Date 15:26 2023/4/24
     * @Param [in]
     * @return com.guang.cms.domain.dto.common.excel.ExcelDataDto
     **/
    public static ExcelDataDto getExcelData(InputStream in) throws IOException {
        Workbook wb = new XSSFWorkbook(in);
        Sheet sheet = wb.getSheetAt(0);
        ExcelDataDto excelData = new ExcelDataDto();
        int rows = sheet.getPhysicalNumberOfRows();
        List<List<Object>> rowList = new ArrayList<>();
        Row firstRow = sheet.getRow(0);
        //列数以第一行为准，其他行可能最后几列存在空列
        int cellCount = firstRow.getLastCellNum();
        for (int i = 0; i < rows; i++) {
            Row row = sheet.getRow(i);
            List<Object> rowData = getRowList(row, cellCount);
            rowList.add(rowData);
        }
        List<List<Object>> dataList = rowList.subList(1, rowList.size());
        excelData.setTitles(rowList.get(0).stream().map(i -> (String) i).collect(Collectors.toList()));
        excelData.setRows(dataList);
        excelData.setName(sheet.getSheetName());

        return excelData;
    }


    private static List<Object> getRowList(Row row,Integer cellCount) {
        //Object[] arr = new Object[row.getLastCellNum()];
        Object[] arr = new Object[cellCount];
        //short lastCellNum = row.getLastCellNum();
        Arrays.fill(arr, "");
        for (int i = 0; i < arr.length; i++) {
            Cell cell = row.getCell(i);
            if (cell == null) {
                continue;
            }
            if (cell.getCellType() == CellType.NUMERIC) {
                if (DateUtil.isCellDateFormatted(cell)) {
                    // 判断单元格是否属于日期格式
                    arr[i] = DateFormatUtils.format(cell.getDateCellValue(), "yyyy-MM-dd HH:mm:ss");
                    continue;
                }
            }
            cell.setCellType(CellType.STRING);
            arr[i] = cell.getStringCellValue();
        }
        return Arrays.asList(arr);
    }
    private static List<Object> getRowList(Row row) {
        return getRowList(row,Integer.valueOf(row.getLastCellNum()));
    }


    public static void exportExcel(ExcelDataDto data, OutputStream out) throws Exception {

        Workbook wb = new XSSFWorkbook();
        try {
            String sheetName = data.getName();
            if (null == sheetName) {
                sheetName = "Sheet1";
            }
            Sheet sheet = wb.createSheet(sheetName);
            writeExcel(wb, sheet, data);

            wb.write(out);
        } finally {
            wb.close();
        }
    }

    private static void writeExcel(Workbook wb, Sheet sheet, ExcelDataDto data) {

        int rowIndex = writeTitlesToExcel(wb, sheet, data.getTitles());
        writeRowsToExcel(wb, sheet, data.getRows(), rowIndex);
        autoSizeColumns(sheet, data.getTitles().size() + 1);

    }

    private static int writeTitlesToExcel(Workbook wb, Sheet sheet, List<String> titles) {
        int rowIndex = 0;
        int colIndex = 0;

        Row titleRow = sheet.createRow(rowIndex);
        for (String field : titles) {
            Cell cell = titleRow.createCell(colIndex);
            cell.setCellValue(field);
            colIndex++;
        }

        rowIndex++;
        return rowIndex;
    }

    private static int writeRowsToExcel(Workbook wb, Sheet sheet, List<List<Object>> rows, int rowIndex) {

        CellStyle dataStyle = wb.createCellStyle();

        int colIndex;
        for (List<Object> rowData : rows) {
            Row dataRow = sheet.createRow(rowIndex);
            colIndex = 0;

            for (Object cellData : rowData) {
                Cell cell = dataRow.createCell(colIndex);
                if (cellData != null) {
                    cell.setCellValue(cellData.toString());
                } else {
                    cell.setCellValue("");
                }

                cell.setCellStyle(dataStyle);
                colIndex++;
            }
            rowIndex++;
        }
        return rowIndex;
    }

    private static void autoSizeColumns(Sheet sheet, int columnNumber) {

        for (int i = 0; i < columnNumber; i++) {
            int orgWidth = sheet.getColumnWidth(i);
            sheet.autoSizeColumn(i, true);
            int newWidth = (int) (sheet.getColumnWidth(i) + 100);
            if (newWidth > orgWidth) {
                sheet.setColumnWidth(i, newWidth);
            } else {
                sheet.setColumnWidth(i, orgWidth);
            }
        }
    }
}
