package com.guang.cms.core.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class PageResponseVo<T> extends ResponseVo<List<T>> implements Serializable {
    @ApiModelProperty("当前页数")
    @JsonProperty("page_num")
    private int pageNum=0;//默认显示第一页
    @ApiModelProperty("数据总数")
    private Long total;
    @ApiModelProperty("总页数")
    private int pages;
    @ApiModelProperty("是否有下一页")
    @JsonProperty("next_page")
    private boolean nextPage;

    @ApiModelProperty("每页显示数据数")
    @JsonProperty("limit")
    private int limit;

    @ApiModelProperty("每页数据数")
    @JsonProperty("page_size")
    private int pageSize;

    public PageResponseVo() {
    }

    public PageResponseVo<T> successPage(List<T> model, int pageIndex, Long totalCount, int pageSize) {
        this.setStatus(true);
        this.setData(model);
        this.setPageNum(pageIndex);
        this.setTotal(totalCount);
        this.setPageSize(pageSize);
        this.setLimit(pageSize);
        this.setPages(getTotalPage());
        this.setNextPage(nextPage());
        return this;
    }

    public static <T> PageResponseVo<T> createPageFailResult(List<T> model, Integer errorCode, String errorMsg) {
        PageResponseVo<T> rt = new PageResponseVo<>();
        rt.setData(model);
        rt.setMsg(errorMsg);
        rt.setErrorCode(errorCode);
        return rt;
    }

    private int getCurrentPage() {
        return this.pageNum < 1 ? 1 : this.pageNum;
    }

    private boolean nextPage() {
        return pages==pageNum?false:true;
    }

    private int getTotalPage() {

        // rows / limit [+1]
        if (total % limit == 0) {
            return (int) (total / limit);
        } else {
            return (int) (total / limit + 1);
        }
    }

    private int getSize() {
        List<T> page = this.getData();
        return page == null ? 0 : page.size();
    }
}