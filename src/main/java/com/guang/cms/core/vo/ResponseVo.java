package com.guang.cms.core.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.guang.cms.core.exception.ErrorCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class ResponseVo<T> implements Serializable {
    /**
     * 默认成功
     */
    @ApiModelProperty("请求是否成功(true:成功 false:失败)")
    private boolean status = true;

    /**
     * 请求返回码:0表示成功
     */
    @ApiModelProperty("请求返回码:0表示成功")
    @JsonProperty("error_code")
    private Integer errorCode = ErrorCode.SUCCESS;

    /**
     * 请求返回说明
     */
    @ApiModelProperty("请求返回说明")
    private String msg = "成功";

    /**
     * 请求返回数据
     */
    @ApiModelProperty("请求返回数据")
    private T data;

    public ResponseVo(T data){
        this.data = data;
    }
    public ResponseVo(Integer errorCode, String msg){
        this.errorCode = errorCode;
        this.msg = msg;
    }
    public ResponseVo(Integer errorCode, String msg, T data){
        this.errorCode = errorCode;
        this.msg = msg;
        this.data = data;
    }
}
