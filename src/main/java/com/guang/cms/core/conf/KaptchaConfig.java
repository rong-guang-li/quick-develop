package com.guang.cms.core.conf;


import com.google.code.kaptcha.Producer;
import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.util.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

@Configuration
public class KaptchaConfig {
    @Bean
    public Producer kaptchaProducer() {

        Properties properties = new Properties();
        properties.setProperty("kaptcha.image.width", "100");//图片宽 100px
        properties.setProperty("kaptcha.image.height", "40");//图片高 40px
        properties.setProperty("kaptcha.textproducer.font.size", "32");//字体大小
        properties.setProperty("kaptcha.textproducer.font.color", "0,0,0");//字体颜色  r,g,b  或者 white,black,blue
        properties.setProperty("kaptcha.textproducer.char.space", "2");//字体间隔
        properties.setProperty("kaptcha.textproducer.char.string", "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYAZ");//随机字符
        properties.setProperty("kaptcha.textproducer.char.length", "4");//字符串长度
        properties.put("kaptcha.noise.color","black"); //干扰颜色 r,g,b 或者 white,black,blue
        properties.put("kaptcha.border.color","blue");//边框颜色 r,g,b (and optional alpha) 或者 white,black,blue(单个或多个)
        properties.put("kaptcha.textproducer.font.names", "Arial,Courier,宋体,楷体,微软雅黑");//文字样式 Arial,Courier,宋体,楷体,微软雅黑
        properties.put("kaptcha.border.thickness", "5");//边框厚度 合法值>1
//        properties.put("kaptcha.background.clear.from", "yellow");//背景颜色渐变，开始颜色
//        properties.put("kaptcha.background.clear.to", "blue");//背景颜色渐变，结束颜色

        //kaptcha.obscurificator.impl设置图片样式
        //水纹  com.google.code.kaptcha.impl.WaterRipple   此时干扰颜色生效
        //鱼眼  com.google.code.kaptcha.impl.FishEyeGimpy  此时干扰颜色不生效
        //阴影  com.google.code.kaptcha.impl.ShadowGimpy   此时干扰颜色生效
//        properties.setProperty("kaptcha.obscurificator.impl", "com.google.code.kaptcha.impl.ShadowGimpy");
        /*
        properties.setProperty("kaptcha.producer.impl", "com.google.code.kaptcha.impl.DefaultKaptcha");//图片实现类
        properties.setProperty("kaptcha.textproducer.impl", "com.google.code.kaptcha.text.impl.DefaultTextCreator");//文本实现类
        properties.setProperty("kaptcha.noise.impl", "com.google.code.kaptcha.impl.DefaultNoise");//干扰实现类
        properties.setProperty("kaptcha.background.impl", "com.google.code.kaptcha.impl.DefaultBackground");//背景实现类
        properties.setProperty("kaptcha.word.impl", "com.google.code.kaptcha.text.impl.DefaultWordRenderer");//文字渲染器
        properties.setProperty("kaptcha.session.key", "KAPTCHA_SESSION_KEY");//session key
        properties.setProperty("kaptcha.session.date", "KAPTCHA_SESSION_DATE");//session date
        */

        DefaultKaptcha kaptcha = new DefaultKaptcha();
        Config config = new Config(properties);
        kaptcha.setConfig(config);

        return kaptcha;
    }
}
