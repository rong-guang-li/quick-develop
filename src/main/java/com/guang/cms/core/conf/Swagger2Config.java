package com.guang.cms.core.conf;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@EnableWebMvc
public class Swagger2Config {

    @Value("${core.swagger2.enable}")
    private boolean swagger2Enable;

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .enable(swagger2Enable)
                .apiInfo(restInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.guang.cms.controller"))
                .paths(PathSelectors.any())
                .build();
    }


    private ApiInfo restInfo() {
        return new ApiInfoBuilder()
                //页面标题
                .title("SpringBoot应用使用Swagger2构建的API")
                //创建人
                .contact(new Contact("lrg", "", ""))
                //版本号
                .version("1.0.1")
                //描述
                .description("接口描述信息")
                .build();
    }
}
