package com.guang.cms.core.conf.security;

import com.guang.cms.core.interceptor.SecurityAuthenticationTokenInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.annotation.Resource;

/**
 * @author lrg
 * @version 1.0
 * @description:
 * @date 2023-07-04  17:37
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)//开启springsecurity注解支持
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Resource
    private SecurityAuthenticationTokenInterceptor securityAuthenticationTokenInterceptor;
    @Resource
    private AccessDeniedHandlerImpl accessDeniedHandler;
    @Resource
    private AuthenticationEntryPointImpl authenticationEntryPoint;

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            //关闭csrf
            .csrf().disable()
            //不通过Session获取SecurityContext
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
            .authorizeRequests()
            // 对于登录接口 允许匿名访问
            .antMatchers("/admin/login/login").anonymous()//允许匿名用户访问,不允许已登入用户访问
            // 除上面外的所有请求全部需要鉴权认证
            .anyRequest().authenticated();

        //将自己设置的拦截器至于UsernamePasswordAuthenticationFilter的前面
        http.addFilterBefore(securityAuthenticationTokenInterceptor, UsernamePasswordAuthenticationFilter.class);

        //配置异常处理器
        http.exceptionHandling()
                .authenticationEntryPoint(authenticationEntryPoint)
                .accessDeniedHandler(accessDeniedHandler);
        //配置允许跨域
        http.cors();
    }

    //认证流程
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
