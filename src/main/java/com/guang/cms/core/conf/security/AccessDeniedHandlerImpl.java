package com.guang.cms.core.conf.security;

import com.alibaba.fastjson.JSON;
import com.guang.cms.core.vo.ResponseVo;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author lrg
 * @version 1.0
 * @description:
 * @date 2023-07-06  09:57
 */

@Component
public class AccessDeniedHandlerImpl implements AccessDeniedHandler {

    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException e) throws IOException, ServletException {

        ResponseVo result = new ResponseVo(403, "权限不足");
        String jsonString = JSON.toJSONString(result);
        PrintWriter writer = response.getWriter();
        writer.write(jsonString);
    }
}