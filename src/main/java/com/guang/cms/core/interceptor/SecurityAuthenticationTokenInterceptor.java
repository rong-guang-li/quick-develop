package com.guang.cms.core.interceptor;

import com.alibaba.fastjson.JSONObject;
import com.guang.cms.core.exception.BizException;
import com.guang.cms.domain.constant.RedisConstant;
import com.guang.cms.domain.dto.admin.security.LoginUser;
import com.guang.cms.domain.entity.AdminUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author lrg
 * @version 1.0
 * @description:
 * @date 2023-07-05  11:06
 */


@Component
public class SecurityAuthenticationTokenInterceptor extends OncePerRequestFilter {

    @Resource
    private RedisTemplate redisTemplate;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String token = request.getHeader("token");

        //放行不需要token就可以访问的接口
        if(token==null||token==""){
            filterChain.doFilter(request,response);
            return;
        }

        //检验token
        LoginUser loginUser = (LoginUser) redisTemplate.boundValueOps(RedisConstant.LOGIN_USER + RedisConstant.SPLIT + token).get();
        if(loginUser==null){
            throw new BizException("用户未登录");
        }

        //将登录用户存入SecurityContextHolder
        //这里必须用三个参数的构造方法，因为super.setAuthenticated(true);进行了授权确认才能接上后面的流程
        UsernamePasswordAuthenticationToken authenticationToken=
                new UsernamePasswordAuthenticationToken(loginUser,null,loginUser.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);

        // TODO: 2023/7/5  将用户信息存入ThreadLocal

        filterChain.doFilter(request,response);
        // TODO: 2023/7/5 把权限信息放入 SecurityContextHolder
    }
}
