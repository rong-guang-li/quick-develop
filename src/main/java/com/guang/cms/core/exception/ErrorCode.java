package com.guang.cms.core.exception;

public class ErrorCode {
    //成功
    public static final int SUCCESS = 100000;

    //未知系统异常
    public static final int UNKNOWN_ERROR = 500000;

    //数据不存在
    public static final int NOT_FIND_DATA = 500001;

    //入参错误
    public static final int ARGS_ERROR = 500002;

    //业务异常
    public static final int BIZ_ERROR = 500003;

    //无效得用户
    public static final int USER_NOT_FOUND_ERROR = 500004;

    //操作过于频繁
    public static final int REQUEST_LIMIT_ERROR = 500005;
}
