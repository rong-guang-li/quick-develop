package com.guang.cms.core.exception;

public class BizException extends RuntimeException {

    /**
     * 错误异常码
     */
    private Integer errorCode;

    public BizException() {
    }
    public BizException(BizExceptionEnum bizExceptionEnum) {
        super(bizExceptionEnum.getName());
        this.errorCode = bizExceptionEnum.getErrorCode();
    }
    public BizException(int errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public BizException(String message) {
        super(message);
        this.errorCode = ErrorCode.BIZ_ERROR;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }
}
