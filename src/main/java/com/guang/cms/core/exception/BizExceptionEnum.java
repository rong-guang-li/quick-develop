package com.guang.cms.core.exception;

public enum BizExceptionEnum {

    //5000000～5000199系统通用异常
    CREATE_FAIL(5000005, "创建失败"),//创建失败
    UPDATE_FAIL(5000006, "更新失败"),//更新失败
    DELETE_FAIL(5000007, "删除失败"),//删除失败
    EMAIL_IS_NOT_TRUE(5000008, "邮箱格式不正确"),//邮箱格式不正确
    DATA_IS_EMPTY(5000009, "数据为空"),//数据为空
    DATA_IS_NOT_EXISTS(5000011, "数据不存在"),//数据不存在
    UPLOAD_FILE_FAIL(5000013, "上传文件失败"),//上传文件失败
    GET_HEADER_FAIL(5000014, "获取头部信息失败"),//获取头部信息失败
    DOWNLOAD_FILE_FAIL(5000015, "下载文件失败"),//下载文件失败
    FILE_IS_NOT_EXISTS(5000016, "文件不存在"),//文件不存在
    FILE_IS_NOT_CORRECT(5000017, "文件不正确"),//文件不正确
    EXPORT_EXCEL_FAIL(5000018, "导出excel文件失败"),//导出excel文件失败
    NOT_OBTAINED_RIGHT_FILE_STREAM(5000022, "没有获取到正确的文件流"),//没有获取到正确的文件流
    OBTAINED_FILE_HASH_ERROR(5000023, "获取文件哈希值失败"),//获取文件哈希值失败
    BSAE64_TO_STR_FAIL(5000024, "base64转化字符串失败"),//base64转化字符串失败
    BASE64_DECODE_FAIL(5000025, "base64解码失败"),//base64解码失败
    BASE64_ENCODE_FAIL(5000026, "base64编码失败"),//base64编码失败
    SEND_MAIL_MESSAGE_FIAL(5000027, "发送邮件消息失败"),//发送邮件消息失败
    NO_PERMISSION(5000028, "无权限"),//无权限查看
    SIGNATURE_ERROR(5000029, "签名错误"),//签名错误
    EXCEL_SUFFIX_ERROR(5000030, "EXCEL_SUFFIX_ERROR"),//EXCEL文件格式错误
    EXCEL_READ_ERROR(5000031, "EXCEL文件格式错误"),//EXCEL读取失败
    EXCEL_WRITE_ERROR(5000032, "EXCEL写入失败"),//EXCEL写入失败
    WRONG_CAPTCHA(5000033, "验证码错误"),
    PASSWORD_IS_NOT_TRUE(5000034, "密码错误"),//密码错误
    USER_IS_NOT_EXISTS(5000035, "用户不存在"),//用户不存在
    NOT_HAVE_PERMISSION(5000036, "NOT_HAVE_PERMISSION"),//没有权限访问
    ACCOUNT_HAVE_BAN(5000037, "ACCOUNT_HAVE_BAN"),//账号被禁用
    ;

    BizExceptionEnum(Integer errorCode, String name) {
        this.errorCode = errorCode;
        this.name = name;
    }

    private Integer errorCode;

    private String name;

    public Integer getErrorCode() {
        return errorCode;
    }

    public String getName() {
        return name;
    }
}
