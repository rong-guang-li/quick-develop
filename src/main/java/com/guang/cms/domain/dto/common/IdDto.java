package com.guang.cms.domain.dto.common;


import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel
public class IdDto {

    @NotNull(message = "id不能为空")
    private Long id;
}
