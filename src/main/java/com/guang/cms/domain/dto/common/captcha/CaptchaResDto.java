package com.guang.cms.domain.dto.common.captcha;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel
public class CaptchaResDto {

    @JsonProperty(value = "captcha_key")
    @ApiModelProperty("验证码key")
    private String captchaKey;

    @ApiModelProperty("验证码")
    private String captcha;

    @ApiModelProperty("验证码base64图片")
    @JsonProperty("captcha_code")
    private String captchaCode;
}
