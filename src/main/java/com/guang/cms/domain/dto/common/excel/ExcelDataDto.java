package com.guang.cms.domain.dto.common.excel;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ExcelDataDto implements Serializable {

    private static final long serialVersionUID = 1949360107346332634L;
    // 表头
    private List<String> titles;

    // 数据
    private List<List<Object>> rows;

    // Sheet名称
    private String name;
}