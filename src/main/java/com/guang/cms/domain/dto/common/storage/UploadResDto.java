package com.guang.cms.domain.dto.common.storage;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UploadResDto {
    @ApiModelProperty("文件url")
    private String url;
    @JsonProperty(value = "file_path")
    @ApiModelProperty("文件路径")
    private String filePath;
}