package com.guang.cms.domain.dto.admin.login;

import lombok.Data;

import java.util.Date;

@Data
public class AdminSessionDto {
    /**
     * token
     */
    private String token;

    /**
     * 账户id
     */
    private Long id=1L;

    /**
     * 最近一次操作时间
     */
    private Date lastTime;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 电话
     */
    private String tel;

    /**
     * 角色类型
     */
    private Integer roleType;

    /**
     * 用户所属服务商ID
     */
    private Long agentId=0L;
}
