package com.guang.cms.domain.dto.admin.menu;


import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel
public class MenuItemListDto {

    @ApiModelProperty("id")
    private Long id;

    @ApiModelProperty("菜单名称")
    private String name;

    @ApiModelProperty("菜单编码")
    @JsonProperty("menu_code")
    private String menuCode;

    @ApiModelProperty("父节点")
    @JsonProperty("parent_id")
    private Long parentId;

    @ApiModelProperty("节点类型，1文件夹，2页面，3按钮")
    @JsonProperty("node_type")
    private Byte nodeType;

    @ApiModelProperty("图标Url")
    @JsonProperty("icon_url")
    private String iconUrl;

    @ApiModelProperty("排序")
    private Integer sort;

    @ApiModelProperty("页面对应的地址")
    @JsonProperty("link_url")
    private String linkUrl;

    @ApiModelProperty("层级")
    private Integer level;

    @ApiModelProperty("树id的路径 整个层次上的路径id，逗号分隔，想要找父节点特别快")
    private String path;

    @ApiModelProperty("子级菜单列表")
    private List<MenuItemListDto> childMenuList;
}
