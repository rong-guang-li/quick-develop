package com.guang.cms.domain.dto.admin.menu;


import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

@Data
@ApiModel
public class MenuSaveDto {

    @ApiModelProperty("id")
    private Long id;

    @ApiModelProperty("菜单名称")
    @NotBlank(message = "菜单名称不能为空")
    private String name;

    @ApiModelProperty("菜单编码")
    @JsonProperty("menu_code")
    @NotBlank(message = "菜单编码不能为空")
    private String menuCode;

    @ApiModelProperty("父节点")
    @JsonProperty("parent_id")
    @NotNull(message = "菜单父节点不能为空")
    private Long parentId;

    @ApiModelProperty("节点类型，1文件夹，2页面，3按钮")
    @JsonProperty("node_type")
    @NotNull(message = "菜单节点类型不能为空")
    private Integer nodeType;

    @ApiModelProperty("图标Url")
    @JsonProperty("icon_url")
    private String iconUrl;

    @ApiModelProperty("排序")
    @NotNull(message = "菜单排序不能为空")
    private Integer sort;

    @ApiModelProperty("页面对应的地址")
    @JsonProperty("link_url")
    private String linkUrl;

    @ApiModelProperty("树id的路径 整个层次上的路径id，逗号分隔，想要找父节点特别快")
    private String path;
}
