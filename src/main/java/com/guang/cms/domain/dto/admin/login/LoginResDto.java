package com.guang.cms.domain.dto.admin.login;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author lrg
 * @version 1.0
 * @description:
 * @date 2023-07-05  09:46
 */
@Data
public class LoginResDto{

    private String token;
    @JsonProperty("role_type")
    private Integer roleType=0;

}
