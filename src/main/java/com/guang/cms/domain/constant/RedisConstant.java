package com.guang.cms.domain.constant;

public class RedisConstant {

    public static final String SPLIT = ":";
    public static final String CAPTCHA_KEY="captcha_key";
    public static final String LOGIN_USER="login_user";
}
