package com.guang.cms.domain.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.guang.cms.domain.dto.admin.menu.MenuItemListDto;
import com.guang.cms.domain.entity.Menu;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 菜单表 Mapper 接口
 * </p>
 *
 * @author lrg
 * @since 2023-04-21 02:57:54
 */
@Repository
public interface MenuMapper extends BaseMapper<Menu> {

    /**
     * @Author lrg
     * @Description 所有的菜单列表
     * @Date 14:56 2023/4/23
     * @Param []
     * @return java.util.List<com.guang.cms.domain.dto.admin.menu.MenuItemListDto>
     **/
    List<MenuItemListDto> selectMenuList();

    /**
     * @Author lrg
     * @Description 检验用户权限
     * @Date 14:58 2023/4/23
     * @Param [userId, menuCode]
     * @return java.lang.Boolean
     **/
    int checkUserAuth(@Param("userId") Long userId,@Param("menuCode") String menuCode);

    /**
     * @Description: 根据用户id查询其所具有的所有的权限（不包括菜单页面权限）
     * @param: userId
     * @return: java.util.List<java.lang.String>
     * @Author: lrg
     * @Date: 2023/7/5
     */
    List<String> queryPermissionByUserId(@Param("userId")Long userId);
}
