package com.guang.cms.domain.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.guang.cms.domain.entity.CoreAttach;
import org.springframework.stereotype.Repository;

@Repository
public interface CoreAttachMapper extends BaseMapper<CoreAttach> {
}
