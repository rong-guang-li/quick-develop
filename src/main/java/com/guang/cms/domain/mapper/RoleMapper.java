package com.guang.cms.domain.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.guang.cms.domain.entity.Role;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author lrg
 * @since 2023-04-21 02:57:54
 */
@Repository
public interface RoleMapper extends BaseMapper<Role> {

}
