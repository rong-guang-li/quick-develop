package com.guang.cms.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 角色表
 * </p>
 *
 * @author lrg
 * @since 2023-04-21 02:57:54
 */
@Getter
@Setter
@TableName("rg_role")
public class Role implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField(value = "name")
    private String name;//角色名称

    @TableField(value = "desc")
    private String desc;//角色描述

    @TableField("operator_num")
    private Integer operatorNum;//操作员数量

    @TableField(value = "status")
    private Integer status;//状态 0禁用 1正常

    @TableField("created_by")
    private Long createdBy;//创建人id
    @TableField(value = "created_at",fill = FieldFill.INSERT)
    private Date createdAt;
    @TableField(value = "updated_at",fill = FieldFill.INSERT_UPDATE)
    private Date updatedAt;

}
