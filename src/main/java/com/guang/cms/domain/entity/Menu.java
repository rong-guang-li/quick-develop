package com.guang.cms.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 菜单表
 * </p>
 *
 * @author lrg
 * @since 2023-04-21 02:57:54
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("rg_menu")
public class Menu implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField(value = "name")
    private String name;//菜单名称

    @TableField(value = "path")
    private String path;//路由地址

    @TableField(value = "component")
    private String component;//组件路径(路由)
    @TableField(value = "visible")
    private Integer visible;//菜单状态 0隐藏 1显示
    @TableField(value = "status")
    private Integer status;//菜单状态 0停用 1正常
    @TableField(value = "permission")
    private String permission;//权限标识
    @TableField(value = "icon")
    private String icon;//图标
    @TableField(value ="pid")
    private Long pid;//父级id
    @TableField(value ="is_delete")
    private Integer isDelete;//是否删除 0未删除 1已删除

    @TableField(value = "created_at",fill = FieldFill.INSERT)
    private Date createdAt;
    @TableField(value = "updated_at",fill = FieldFill.INSERT_UPDATE)
    private Date updatedAt;
}
