package com.guang.cms.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * <p>
 * 用户—角色中间表
 * </p>
 *
 * @author lrg
 * @since 2023-04-21 02:57:54
 */
@Getter
@Setter
@TableName("rg_user_role_mid")
public class UserRoleMid implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField(value = "user_id")
    private Long userId;//用户id

    @TableField(value = "role_id")
    private Long roleId;//角色id
}
