package com.guang.cms.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("rg_admin_user")
public class AdminUser implements Serializable {

    @TableId(value = "id",type = IdType.AUTO)
    private Long id;
    @TableField(value = "username")
    private String username;//用户名
    @TableField(value = "nick_name")
    private String nickName;//用户名
    @TableField(value = "email")
    private String email;//登录邮箱
    @TableField(value = "password")
    private String password;//密码
    @TableField(value = "salt")
    private String salt;//密码加密盐值

    @TableField(value = "status")
    private Integer status;//状态 0禁用1正常
    @TableLogic
    @TableField(value = "is_delete",fill = FieldFill.INSERT)
    private Integer isDelete;//是否已删除

    @TableField(value = "last_login_time")
    private Date lastLoginTime;//上一次登录时间
    @TableField(value = "created_at",fill = FieldFill.INSERT)
    private Date createdAt;
    @TableField(value = "updated_at",fill = FieldFill.INSERT_UPDATE)
    private Date updatedAt;

    public static final Long serialVersionUID=1L;
}
