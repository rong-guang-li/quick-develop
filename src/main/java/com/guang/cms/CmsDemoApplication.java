package com.guang.cms;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

@SpringBootApplication
@MapperScan("com.guang.cms.domain.mapper")
public class CmsDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(CmsDemoApplication.class, args);
    }

}
