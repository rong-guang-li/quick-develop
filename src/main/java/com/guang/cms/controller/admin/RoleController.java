package com.guang.cms.controller.admin;


import com.guang.cms.core.vo.ResponseVo;
import com.guang.cms.domain.dto.admin.menu.MenuSaveDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author lrg
 * @since 2023-04-21 02:57:54
 */
@RestController
@RequestMapping("/admin/role")
@Api(tags = "管理端角色相关")
public class RoleController {

    @PostMapping("/save")
    @PreAuthorize("hasAuthority('admin.role.save')")
    @ApiOperation(value = "保存角色", notes = "保存角色")
    public ResponseVo save() {
        return new ResponseVo();
    }

    @PostMapping("/delete")
    @PreAuthorize("hasAuthority('admin.role.delete')")
    @ApiOperation(value = "删除角色", notes = "删除角色")
    public ResponseVo delete() {
        return new ResponseVo();
    }

    @PostMapping("/list")
    @PreAuthorize("hasAuthority('admin.role.list')")
    @ApiOperation(value = "角色列表", notes = "角色列表")
    public ResponseVo list() {
        return new ResponseVo();
    }

    @PostMapping("/check")
    @PreAuthorize("hasAuthority('admin.role.check')")
    @ApiOperation(value = "角色审核", notes = "角色审核")
    public ResponseVo check() {
        return new ResponseVo();
    }

}
