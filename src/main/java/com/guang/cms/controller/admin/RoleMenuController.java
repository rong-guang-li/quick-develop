package com.guang.cms.controller.admin;

import com.guang.cms.core.vo.ResponseVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 角色-菜单中间表 前端控制器
 * </p>
 *
 * @author lrg
 * @since 2023-04-21 02:57:54
 */
@RestController
@RequestMapping("/roleMenuMgr")
@Api("管理端角色的菜单权限相关")
public class RoleMenuController {

    @PostMapping("/save")
    @ApiOperation(value = "保存角色的菜单权限", notes = "保存角色的菜单权限")
    public ResponseVo save() {
        return new ResponseVo();
    }

    @PostMapping("/delete")
    @ApiOperation(value = "删除角色的菜单权限", notes = "删除角色的菜单权限")
    public ResponseVo delete() {
        return new ResponseVo();
    }

    @PostMapping("/list")
    @ApiOperation(value = "角色的菜单权限列表", notes = "角色的菜单权限列表")
    public ResponseVo list() {
        return new ResponseVo();
    }
}
