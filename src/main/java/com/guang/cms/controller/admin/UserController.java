package com.guang.cms.controller.admin;


import com.guang.cms.core.vo.ResponseVo;
import com.guang.cms.domain.dto.admin.menu.MenuSaveDto;
import com.guang.cms.domain.dto.common.IdDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author lrg
 * @since 2023-04-21 02:57:54
 */
@RestController
@RequestMapping("/admin/user")
@Api(tags = "管理端用户相关")
public class UserController {


    @PostMapping("/save")
    @ApiOperation(value = "保存用户", notes = "保存用户")
    public ResponseVo save(MenuSaveDto menuSaveDto) {
        return new ResponseVo();
    }

    @PostMapping("/delete")
    @ApiOperation(value = "删除用户", notes = "删除用户")
    public ResponseVo delate(IdDto idDto) {
        return new ResponseVo();
    }

    @PostMapping("/list")
    @ApiOperation(value = "用户列表", notes = "用户列表")
    public ResponseVo list() {
        return new ResponseVo();
    }
}
