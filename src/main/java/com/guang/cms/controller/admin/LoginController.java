package com.guang.cms.controller.admin;

import com.guang.cms.core.vo.ResponseVo;
import com.guang.cms.domain.dto.admin.login.LoginResDto;
import com.guang.cms.domain.entity.AdminUser;
import com.guang.cms.service.admin.ILoginService;
import com.guang.cms.service.common.ICommonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author lrg
 * @version 1.0
 * @description:
 * @date 2023-07-05  09:09
 */

@RestController
@RequestMapping("/admin/login")
@Api(tags = "管理端登录接口")
public class LoginController {

    @Resource
    ILoginService loginService;

    @PostMapping("/login")
    @ApiOperation(value = "登录",notes = "登录")
    public ResponseVo<LoginResDto> login(@RequestBody AdminUser adminUser){
        LoginResDto loginResDto=loginService.login(adminUser.getUsername(),adminUser.getPassword());
        return new ResponseVo(loginResDto);
    }

    @PostMapping("/logout")
    @ApiOperation(value = "登出",notes = "登出")
    public ResponseVo logout(HttpServletRequest request){
        loginService.logout(request);
        return new ResponseVo();
    }
}
