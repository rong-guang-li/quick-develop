package com.guang.cms.controller.admin;


import com.guang.cms.core.vo.ResponseVo;
import com.guang.cms.domain.dto.admin.menu.MenuItemListDto;
import com.guang.cms.domain.dto.admin.menu.MenuSaveDto;
import com.guang.cms.domain.dto.common.IdDto;
import com.guang.cms.service.admin.IMenuService;
import com.guang.cms.service.admin.impl.MenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 菜单表 前端控制器
 * </p>
 *
 * @author lrg
 * @since 2023-04-21 02:57:54
 */
@RestController
@RequestMapping("/admin/menu")
@Api(tags = "管理端菜单")
public class MenuController {

    @Resource
    IMenuService menuService;

    @PostMapping("/save")
    @PreAuthorize("hasAnyAuthority('admin.menu.save')")
    @ApiOperation(value = "保存菜单", notes = "保存菜单")
    public ResponseVo save(MenuSaveDto menuSaveDto) {
        menuService.save(menuSaveDto);
        return new ResponseVo();
    }

    @PostMapping("/delete")
    @PreAuthorize("hasAnyAuthority('admin.menu.delete')")
    @ApiOperation(value = "删除菜单", notes = "删除菜单")
    public ResponseVo delate(IdDto idDto) {
        menuService.delate(idDto);
        return new ResponseVo();
    }

    @PostMapping("/list")
    @PreAuthorize("hasAnyAuthority('admin.menu.list')")
    @ApiOperation(value = "菜单列表", notes = "菜单列表")
    public ResponseVo<List<MenuItemListDto>> list(Long userId) {
        List<MenuItemListDto> result=menuService.list();
        return new ResponseVo(result);
    }
}
