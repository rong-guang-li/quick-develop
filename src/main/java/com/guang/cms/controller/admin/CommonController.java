package com.guang.cms.controller.admin;


import com.guang.cms.core.utils.CommonUtil;
import com.guang.cms.core.utils.DateUtil;
import com.guang.cms.core.vo.ResponseVo;
import com.guang.cms.domain.dto.common.captcha.CaptchaResDto;
import com.guang.cms.service.common.ICommonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/admin/common")
@Api(tags = "管理端通用功能")
public class CommonController {

    @Resource
    ICommonService commonService;

    @PostMapping("/upload")
    @ApiOperation(value = "文件上传",notes = "文件上传")
    public ResponseVo uplaod(@Param("file") MultipartFile file){
        commonService.upload(file);
        return new ResponseVo();
    }

    /*
     *参考：https://blog.csdn.net/NineWaited/article/details/126995940?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522168258627716800217276300%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=168258627716800217276300&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~baidu_landing_v2~default-1-126995940-null-null.142^v86^koosearch_v1,239^v2^insert_chatgpt&utm_term=%E5%88%86%E7%89%87%E4%B8%8A%E4%BC%A0%E5%90%8E%E7%AB%AF%E5%AE%9E%E7%8E%B0&spm=1018.2226.3001.4187
     *
     */

    /**
     * @Author lrg
     * @Description
     * @Date 11:37 2023/4/24
     * @Param [fileName, response] fileName是数据库中存储的文件名
     * @return org.springframework.http.ResponseEntity<byte[]>
     **/
    @ApiOperation(value = "文件访问/下载", notes = "文件访问/下载")
    @RequestMapping(value = "/download", method = RequestMethod.GET)
    public ResponseEntity<byte[]> downloadFile(@RequestParam("fileName") String fileName, HttpServletResponse response) {
        ResponseEntity responseEntity = commonService.download(fileName);
        if(null!=responseEntity){
            return responseEntity;
        }else{
            // 下载失败直接返回错误的请求
            return (ResponseEntity<byte[]>) ResponseEntity.badRequest();
        }
    }

    @PostMapping("/upload_part")
    @ApiOperation(value = "文件上传",notes = "文件上传")
    public ResponseVo multipartUpload(@Param("file")MultipartFile file){

        return null;
    }


    @Value("${test.file.storage.save.path.suffix}")
    private String filePath;

    /**
     * @Author lrg
     * @Description 文件分片上传
     * @Date 9:15 2023/4/28
     * @Param
     * @param file 文件
     * @param chunkNumber 第几个分片
     * @param totalChunks 分片总数
     * @param totalSize 全部大小
     * @param filename 文件名称
     * @param curUrl
     * @param request
     * @return void
     **/
    @PostMapping("/uploadFile")
    @ResponseBody
    public void uploadFile( @RequestParam("file") MultipartFile file,
                            @RequestParam("chunkNumber") Integer chunkNumber,
                            @RequestParam("totalChunks") Integer totalChunks,
                            @RequestParam("totalSize") String totalSize,
                            @RequestParam("filename") String filename,
                            @RequestParam("curUrl") String curUrl,
                            HttpServletRequest request) {

        //对于localUrl，如果不是末尾分片，我们应该加上一个tmp文件夹避免文件混乱。
        //只有发起合并请求的时候再合并到源路径后删除tmp文件夹。
        //注意，.需要转义
        String localUrl = filePath + DateUtil.getCurrDate().getTime();
        //不是最后一个分片，不需要合并
        commonService.uploadFile(file, localUrl, "" +chunkNumber, false);
        if(chunkNumber == totalChunks) {
            //否则发起合并服务,merge合并
            commonService.uploadFile(file, localUrl, filename, true);
        }
    }


    @PostMapping("/captcha")
    @ApiOperation(value = "验证码",notes = "验证码")
    public ResponseVo<CaptchaResDto> getCaptcha(){
        CaptchaResDto result=commonService.getCaptcha();
        return new ResponseVo(result);
    }
}
