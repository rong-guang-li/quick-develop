package com.guang.cms.controller.admin;

import com.guang.cms.core.vo.ResponseVo;
import com.guang.cms.domain.dto.admin.menu.MenuSaveDto;
import com.guang.cms.domain.dto.common.IdDto;
import com.guang.cms.service.admin.IRoleService;
import com.guang.cms.service.admin.impl.RoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 用户—角色中间表 前端控制器
 * </p>
 *
 * @author lrg
 * @since 2023-04-21 02:57:54
 */
@RestController
@RequestMapping("/admin/user_role")
@Api(tags = "管理端用户角色相关")
public class UserRoleController {

    @Resource
    IRoleService roleService;

    @PostMapping("/save")
    @ApiOperation(value = "保存用户的角色类型", notes = "保存用户的角色类型")
    public ResponseVo save(MenuSaveDto menuSaveDto) {
        roleService.save(menuSaveDto);
        return new ResponseVo();
    }

    @PostMapping("/delete")
    @ApiOperation(value = "删除用户的角色类型", notes = "删除用户的角色类型")
    public ResponseVo delate(IdDto idDto) {
        roleService.delete(idDto.getId());
        return new ResponseVo();
    }

    @PostMapping("/list")
    @ApiOperation(value = "用户的角色列表", notes = "用户的角色列表")
    public ResponseVo list() {
        roleService.list();
        return new ResponseVo();
    }
}
