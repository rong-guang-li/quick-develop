package com.guang.cms.service.common;

import com.guang.cms.domain.dto.common.storage.UploadResDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

public interface IStorageService {
    UploadResDto upload(MultipartFile file);

    ResponseEntity download(String fileName);

    void uploadFile(MultipartFile file, String localUrl, String filename, boolean merge);

    Long findNewShard(String localUrl);
}
