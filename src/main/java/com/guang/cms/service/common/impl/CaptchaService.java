package com.guang.cms.service.common.impl;

import com.google.code.kaptcha.Producer;
import com.guang.cms.core.utils.CommonUtil;
import com.guang.cms.core.utils.ImageUtil;
import com.guang.cms.domain.constant.RedisConstant;
import com.guang.cms.domain.dto.common.captcha.CaptchaResDto;
import com.guang.cms.service.common.ICaptchaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.concurrent.TimeUnit;


@Service
@Slf4j
public class CaptchaService implements ICaptchaService {

    @Resource
    Producer kaptchaProducer;
    @Resource
    RedisTemplate redisTemplate;

    /**
     * @Author lrg
     * @Description 获取相同颜色不同样式的验证码
     * @Date 13:56 2023/4/24
     * @Param []
     * @return com.guang.cms.domain.dto.common.captcha.CaptchaResDto
     **/
    @Override
    public CaptchaResDto getSameColorCaptcha() {

        String code = kaptchaProducer.createText();
        BufferedImage image = kaptchaProducer.createImage(code);
        String uuid = CommonUtil.getUUID();
        //验证码存储65秒
        redisTemplate.boundValueOps(RedisConstant.CAPTCHA_KEY+uuid).set(code,65,TimeUnit.SECONDS);

        CaptchaResDto captchaResDto = new CaptchaResDto();
        captchaResDto.setCaptchaKey(uuid);
        captchaResDto.setCaptcha(code);
        captchaResDto.setCaptchaCode(ImageUtil.toBase64(image));
        return captchaResDto;
    }

    /**
     * @Author lrg
     * @Description 获取不同颜色的验证码
     * @Date 13:56 2023/4/24
     * @Param []
     * @return com.guang.cms.domain.dto.common.captcha.CaptchaResDto
     **/
    @Override
    public CaptchaResDto getDiffColorCaptcha() {
        BufferedImage image=new BufferedImage(100, 40, BufferedImage.TYPE_3BYTE_BGR);//设置生成的图像的大小，单位px

        Graphics g=image.getGraphics();//获取绘图对象
        g.setColor(new Color(239, 239, 239));//设置背景颜色
        g.fillRect(0,0,100,40);//背景填充区域

        //绘制字符串:每次画一个字符
        int space=2;
        String code="";
        for(int i=0;i<4;i++) {
            g.setColor(new Color((int) (Math.random()*255), (int) (Math.random()*255), (int) (Math.random()*255)));
            String text =String.valueOf((int) (Math.random()*10));//随机的一位验证码
            code+=text;

            g.setFont(new Font("SimSong", Font.ITALIC,32));//设置字体样式，大小20px
            g.drawString(text, space+5, 32);//每次打印一个验证码，得控制好坐标
            space+=20;
        }
        log.info("验证码----------------+："+code);
        //可以将验证码一次性全部打印

        //画干扰线条
        for (int i = 0; i < 10; i++) {
            g.setColor(new Color((int) (Math.random()*255), (int) (Math.random()*255), (int) (Math.random()*255)));
            g.drawLine((int) (Math.random()*50),(int) (Math.random()*30),(int) (Math.random()*80),(int) (Math.random()*80));
        }

        g.dispose();

        String uuid = CommonUtil.getUUID();
        //将验证码放入redis,65秒方案
        redisTemplate.boundValueOps(RedisConstant.CAPTCHA_KEY+uuid).set(code, 65, TimeUnit.HOURS);

        CaptchaResDto captchaResDto = new CaptchaResDto();
        captchaResDto.setCaptchaKey(uuid);
        captchaResDto.setCaptchaCode(ImageUtil.toBase64(image));
        captchaResDto.setCaptcha(code);
        //将图片输出给浏览器
        /*response.setContentType("image/png");
        try {
            ServletOutputStream os = response.getOutputStream();
            ImageIO.write(image, "png", os);
        } catch (IOException e) {
            throw new BizException(BizExceptionEnum.GET_PICTURE_FAIL);
        }*/
        return captchaResDto;
    }
}
