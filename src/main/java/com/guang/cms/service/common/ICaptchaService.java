package com.guang.cms.service.common;

import com.guang.cms.domain.dto.common.captcha.CaptchaResDto;

public interface ICaptchaService {

    CaptchaResDto getSameColorCaptcha();
    CaptchaResDto getDiffColorCaptcha();
}
