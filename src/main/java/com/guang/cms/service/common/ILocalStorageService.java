package com.guang.cms.service.common;


import com.guang.cms.domain.dto.common.storage.UploadResDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


public interface ILocalStorageService {

    UploadResDto upload(MultipartFile file);

    ResponseEntity download(String fileName);
}
