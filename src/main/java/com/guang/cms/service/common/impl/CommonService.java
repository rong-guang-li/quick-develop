package com.guang.cms.service.common.impl;


import com.guang.cms.domain.dto.common.captcha.CaptchaResDto;
import com.guang.cms.domain.dto.common.storage.UploadResDto;
import com.guang.cms.service.common.ICaptchaService;
import com.guang.cms.service.common.ICommonService;
import com.guang.cms.service.common.IStorageService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;


@Service
public class CommonService implements ICommonService {

    @Resource
    IStorageService storageService;
    @Resource
    ICaptchaService captchaService;

    /**
     * @Author lrg
     * @Description 文件上传
     * @Date 10:59 2023/4/24
     * @Param [file]
     * @return com.guang.cms.domain.dto.common.storage.UploadResDto
     **/
    @Override
    public UploadResDto upload(MultipartFile file){
        return storageService.upload(file);
    }

    /**
     * @Author lrg
     * @Description 下载文件
     * @Date 11:03 2023/4/24
     * @Param [fileName]
     * @return org.springframework.http.ResponseEntity
     **/
    @Override
    public ResponseEntity download(String fileName) {
        return storageService.download(fileName);
    }

    /**
     * @Author lrg
     * @Description 获取验证码
     * @Date 13:51 2023/4/24
     * @Param []
     * @return com.guang.cms.domain.dto.common.captcha.CaptchaResDto
     **/
    @Override
    public CaptchaResDto getCaptcha() {
        return captchaService.getDiffColorCaptcha();
    }


    @Override
    public void uploadFile(MultipartFile file, String localUrl, String filename, boolean merge) {
       storageService.uploadFile(file,localUrl,filename,merge);
    }

    @Override
    public Long findNewShard(String localUrl) {
        return storageService.findNewShard(localUrl);
    }
}
