package com.guang.cms.service.common.impl;

import com.guang.cms.core.exception.BizException;
import com.guang.cms.core.exception.BizExceptionEnum;
import com.guang.cms.core.utils.CommonUtil;
import com.guang.cms.core.utils.DateUtil;
import com.guang.cms.domain.dto.common.storage.UploadResDto;
import com.guang.cms.domain.entity.CoreAttach;
import com.guang.cms.domain.mapper.CoreAttachMapper;
import com.guang.cms.service.common.ILocalStorageService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.concurrent.TimeUnit;


@Service
@Slf4j
public class LocalStorageService implements ILocalStorageService {

//    @Value("${storage.save.path.suffix}") //服务器上使用该路径
    @Value("${test.storage.save.path.suffix}") //本地测试使用该路径
    private String fileSavePrefix;
    @Resource
    CoreAttachMapper coreAttachMapper;

    /**
     * @Author lrg
     * @Description 上传文件到本地
     * @Date 17:36 2023/4/23
     * @Param [file]
     * @return com.guang.cms.domain.dto.common.storage.UploadResDto
     **/
    @Override
    public UploadResDto upload(MultipartFile file) {

        if(file==null){
            throw new BizException(BizExceptionEnum.NOT_OBTAINED_RIGHT_FILE_STREAM);
        }

        String filename = file.getOriginalFilename();//文件的名称及其后缀
        String suffix = filename.substring(filename.lastIndexOf("."));//文件后缀，例如.png
        if (StringUtils.isBlank(suffix)) {
            throw new BizException(BizExceptionEnum.FILE_IS_NOT_CORRECT);
        }

        String saveFileName = CommonUtil.getUUID()+suffix;
        String saveFileDateDir = DateUtil.getDateStr();

        String fullName=saveFileDateDir+"/"+saveFileName;
        String filePath=fileSavePrefix+fullName;
        File dest=new File(filePath);

        try {
            //创建上传的文件夹后上传文件
            dest.mkdirs();
            file.transferTo(dest);
        } catch (IOException e) {
            log.error("上传文件失败" + e.getMessage());
            throw new BizException(BizExceptionEnum.UPLOAD_FILE_FAIL);
        }

        //存储文件
        CoreAttach coreAttach = new CoreAttach();
        coreAttach.setFileName(saveFileName);
        coreAttach.setFilePath(fullName);
        coreAttachMapper.insert(coreAttach);

        //返回上传相关反馈信息
        UploadResDto uploadResDto = new UploadResDto();
        uploadResDto.setFilePath(fullName);
//        uploadResDto.setUrl();
        return uploadResDto;
    }

    /**
     * @Author lrg
     * @Description 下载文件
     * @Date 11:10 2023/4/24
     * @Param [fileName] 数据库中存储的fileName
     * @return org.springframework.http.ResponseEntity
     **/
    @Override
    public ResponseEntity download(String fileName) {

        File file = new File(fileSavePrefix + "/"+ fileName);

        ResponseEntity.BodyBuilder bodyBuilder = ResponseEntity.ok();
        bodyBuilder.contentLength(file.length());
        // 二进制数据流
        bodyBuilder.contentType(MediaType.APPLICATION_OCTET_STREAM);
        // 文件名编码
        try {
            String encodeFileName = URLEncoder.encode(fileName, "UTF-8");
            bodyBuilder.header("Content-Disposition","attachment;filename*=UTF-8''"+encodeFileName);
            // 下载成功返回二进制流
            return bodyBuilder.body(FileUtils.readFileToByteArray(file));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 有签名鉴权
     * @param fileName
     * @return
     */
    public static final String PUBLIC_DOWNLOAD_URL_PRE = "/api/admin/public/file/";//公共文件访问下载地址前缀,有签名鉴权
//    @Override
//    public String getSigUrl(String fileName){
//        Long adminId = AdminSessionUtil.getId();
//        String url;
//        String cacheKey;
//        String signature;
//        signature = MD5Util.lowerCaseMD5(String.valueOf(adminId)+fileName);
//        cacheKey = RedisConst.LOCAL_STORAGE_SIG+signature;
//        url = this.domain+this.contextPath+ PUBLIC_DOWNLOAD_URL_PRE+this.fileParamPrix+fileName;
//        redisTemplate.boundValueOps(cacheKey).set("1",CommonConst.STORAGE_URL_DEFAULT_EXPIRATION, TimeUnit.SECONDS);
//        url = url+sigParamPrix+signature;
//        return url;
//    }
}
