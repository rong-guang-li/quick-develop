package com.guang.cms.service.common.impl;

import com.guang.cms.core.utils.MultipartFileUtil;
import com.guang.cms.domain.dto.common.storage.UploadResDto;
import com.guang.cms.service.common.IStorageService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Service
public class StorageService implements IStorageService {

    @Resource
    LocalStorageService localStorageService;

    /**
     * @return com.guang.cms.domain.dto.common.storage.UploadResDto
     * @Author lrg
     * @Description 文件上传
     * @Date 9:47 2023/4/24
     * @Param [file]
     **/
    @Override
    public UploadResDto upload(MultipartFile file) {
        return localStorageService.upload(file);
    }

    /**
     * @return org.springframework.http.ResponseEntity
     * @Author lrg
     * @Description 下载文件
     * @Date 11:09 2023/4/24
     * @Param [fileName]
     **/
    @Override
    public ResponseEntity download(String fileName) {
        return localStorageService.download(fileName);
    }

    /**
     * @Author lrg
     * @Description 上传文件
     * @Date 17:49 2023/4/27
     * @Param [file, localUrl, filename, merge]二进制文件流，也就是目标文件，上传的目标地址
     * @return void
     **/
    @Override
    public void uploadFile(MultipartFile file, String localUrl, String filename, boolean merge) {
        if (!merge) {
            MultipartFileUtil.addFile(file, localUrl, filename);
        } else {
            MultipartFileUtil.mergeFileByRandomAccessFile(localUrl, filename);
            //合并后删除tmp文件夹
            try {
                MultipartFileUtil.deleteDirByNio(localUrl);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @Author lrg
     * @Description 找到最新的分片编号
     * @Date 17:50 2023/4/27
     * @Param [localUrl]临时文件夹位置
     * @return java.lang.Long
     **/
    @Override
    public Long findNewShard(String localUrl) {

        File tempDir = new File(localUrl);
        //如果连临时文件夹都没有，说明一定还没开始上传，不需要续传，直接0
        if (!tempDir.exists()) {
            tempDir.mkdirs();
            return 0L;
        }
        //应该检查目前到第几个分片，默认分片是有序的
        long count = 0;
        try {
            count = Files.list(Paths.get(localUrl)).count();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }

}
