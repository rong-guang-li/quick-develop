package com.guang.cms.service.admin.impl;

import com.guang.cms.core.exception.BizException;
import com.guang.cms.core.utils.CommonUtil;
import com.guang.cms.domain.constant.RedisConstant;
import com.guang.cms.domain.dto.admin.login.LoginResDto;
import com.guang.cms.domain.dto.admin.security.LoginUser;
import com.guang.cms.domain.entity.AdminUser;
import com.guang.cms.service.admin.ILoginService;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

/**
 * @author lrg
 * @version 1.0
 * @description:
 * @date 2023-07-05  09:34
 */
@Service
public class LoginService implements ILoginService {

    @Resource
    private AuthenticationManager authenticationManager;
    @Resource
    private RedisTemplate redisTemplate;

    /**
     * @Description: 登录接口
     * @param: username
     * @param: password
     * @return: void
     * @Author: lrg
     * @Date: 2023/7/5
     */
    @Override
    public LoginResDto login(String username, String password) {

        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, password);
        Authentication authenticate = authenticationManager.authenticate(authenticationToken);
        if(null==authenticate){
            throw new BizException("用户名或密码错误");
        }

        LoginUser loginUser = (LoginUser) authenticate.getPrincipal();

        //认证成功，生成token,将用户存储在redis
        String token = CommonUtil.getUUID();
        redisTemplate.boundValueOps(RedisConstant.LOGIN_USER+RedisConstant.SPLIT+token).set(loginUser);

        LoginResDto loginResDto = new LoginResDto();
        loginResDto.setToken(token);
        return loginResDto;
    }

    /**
     * @Description: 登出
     * @param
     * @return: void
     * @Author: lrg
     * @Date: 2023/7/5
     */
    @Override
    public void logout(HttpServletRequest request) {
        String token = request.getHeader("token");
        redisTemplate.delete(RedisConstant.LOGIN_USER+RedisConstant.SPLIT+token);
        
        //// TODO: 2023/7/5  删除Cookie、删除ThreadLocal
    }
}
