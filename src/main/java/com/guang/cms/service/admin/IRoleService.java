package com.guang.cms.service.admin;


import com.guang.cms.domain.dto.admin.menu.MenuSaveDto;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author lrg
 * @since 2023-04-21 02:57:54
 */
public interface IRoleService{

    void save(MenuSaveDto menuSaveDto);

    void delete(Long id);

    void list();
}
