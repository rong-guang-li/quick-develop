package com.guang.cms.service.admin.impl;


import com.guang.cms.service.admin.IUserRoleService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户—角色中间表 服务实现类
 * </p>
 *
 * @author lrg
 * @since 2023-04-21 02:57:54
 */
@Service
public class UserRoleService  implements IUserRoleService {

}
