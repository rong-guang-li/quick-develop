package com.guang.cms.service.admin.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.guang.cms.core.exception.BizException;
import com.guang.cms.core.utils.CommonUtil;
import com.guang.cms.domain.dto.admin.menu.MenuItemListDto;
import com.guang.cms.domain.dto.admin.menu.MenuSaveDto;

import com.guang.cms.domain.dto.common.IdDto;
import com.guang.cms.domain.entity.Menu;
import com.guang.cms.domain.entity.RoleMenuMid;
import com.guang.cms.domain.mapper.MenuMapper;
import com.guang.cms.domain.mapper.RoleMenuMidMapper;
import com.guang.cms.service.admin.IMenuService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 菜单表 服务实现类
 * </p>
 *
 * @author lrg
 * @since 2023-04-21 02:57:54
 */
@Service
public class MenuService implements IMenuService {

    @Resource
    MenuMapper menuMapper;
    @Resource
    RoleMenuMidMapper roleMenuMapper;

    /**
     * @return void
     * @Author lrg
     * @Description 保存菜单
     * @Date 15:29 2023/4/21
     * @Param []
     **/
    @Override
    public void save(MenuSaveDto menuSaveDto) {

        if (menuSaveDto.getId() == null) {
            saveMenu(menuSaveDto);
        } else {
            update(menuSaveDto);
        }
    }

    /**
     * @return void
     * @Author lrg
     * @Description 删除菜单
     * @Date 15:29 2023/4/21
     * @Param []
     **/
    @Override
    public void delate(IdDto idDto) {
        //菜单存在角色关联则无法删除

        QueryWrapper<RoleMenuMid> wrapper = new QueryWrapper<>();
        wrapper.eq("menu_id", idDto.getId());

        RoleMenuMid roleMenu = roleMenuMapper.selectOne(wrapper);
        if (null == roleMenu) {
            menuMapper.deleteById(idDto.getId());
        }
        throw new BizException("菜单存在角色绑定，不能删除");
    }

    /**
     * @return void
     * @Author lrg
     * @Description 菜单列表
     * @Date 15:30 2023/4/21
     * @Param []
     **/
    @Override
    public List<MenuItemListDto> list() {
        List<MenuItemListDto> result = menuMapper.selectMenuList();
        if (CommonUtil.listNotEmpty(result)) ;

        List<MenuItemListDto> data = new ArrayList<>();
        for (MenuItemListDto dto : result) {

            if (null == dto) {
                continue;
            }
            if (dto.getLevel() == 1) {
                data.add(dto);
            } else {
                setChildMenu(dto, data);
            }
        }
        return data;
    }

    /**
     * @return void
     * @Author lrg
     * @Description 新增菜单
     * @Date 16:56 2023/4/25
     * @Param [menuSaveDto]
     **/
    public void saveMenu(MenuSaveDto menuSaveDto) {
        Menu menu = new Menu();
        Long parentId = menuSaveDto.getParentId();
        Menu parentMenu = menuMapper.selectById(parentId);

        BeanUtils.copyProperties(menuSaveDto, menu);
        menuMapper.insert(menu);
    }

    /**
     * @return java.lang.String
     * @Author lrg
     * @Description 更新菜单
     * @Date 16:57 2023/4/25
     * @Param [menuSaveDto]
     **/
    public void update(MenuSaveDto menuSaveDto) {
        Long id = menuSaveDto.getId();
        Menu menu = menuMapper.selectById(id);

        BeanUtils.copyProperties(menuSaveDto, menu);
        menuMapper.updateById(menu);
    }

    /**
     * @return void
     * @Author lrg
     * @Description 设置该菜单的子级菜单
     * @Date 14:08 2023/4/23
     * @Param [dto, data]
     **/
    private void setChildMenu(MenuItemListDto dto, List<MenuItemListDto> data) {

        List<MenuItemListDto> childMenuList;
        if (CommonUtil.listNotEmpty(data)) {
            for (MenuItemListDto item : data) {

                if (CommonUtil.listNotEmpty(item.getChildMenuList())) {
                    childMenuList = item.getChildMenuList();
                } else {
                    childMenuList = new ArrayList<>();
                }

                List<MenuItemListDto> childItemMenuList = item.getChildMenuList();
                if (dto.getParentId().equals(item.getId())) {
                    childMenuList.add(dto);
                } else if (CommonUtil.listNotEmpty(childItemMenuList)) {
                    setChildMenu(dto, childItemMenuList);
                }

                item.setChildMenuList(childMenuList);
            }
        }
    }
}
