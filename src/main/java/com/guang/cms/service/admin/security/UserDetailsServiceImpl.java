package com.guang.cms.service.admin.security;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.guang.cms.core.exception.BizException;
import com.guang.cms.domain.dto.admin.security.LoginUser;
import com.guang.cms.domain.entity.AdminUser;
import com.guang.cms.domain.mapper.AdminUserMapper;
import com.guang.cms.domain.mapper.MenuMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * @author lrg
 * @version 1.0
 * @description:
 * @date 2023-07-04  17:32
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Resource
    private AdminUserMapper adminUserMapper;
    @Resource
    private MenuMapper menuMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        LambdaQueryWrapper<AdminUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AdminUser::getUsername,username);
        AdminUser adminUser = adminUserMapper.selectOne(queryWrapper);
        if(adminUser==null){
            throw new BizException("用户不存在");
        }

        List<String> permissions = menuMapper.queryPermissionByUserId(adminUser.getId());
        return new LoginUser(adminUser,permissions);
    }
}
