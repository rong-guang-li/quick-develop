package com.guang.cms.service.admin;


import com.guang.cms.domain.dto.admin.menu.MenuItemListDto;
import com.guang.cms.domain.dto.admin.menu.MenuSaveDto;
import com.guang.cms.domain.dto.common.IdDto;

import java.util.List;

/**
 * <p>
 * 菜单表 服务类
 * </p>
 *
 * @author lrg
 * @since 2023-04-21 02:57:54
 */
public interface IMenuService{

    void save(MenuSaveDto menuSaveDto);

    void delate(IdDto idDto);

    List<MenuItemListDto> list();
}
