package com.guang.cms.service.admin.impl;


import com.guang.cms.service.admin.IUserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author lrg
 * @since 2023-04-21 02:57:54
 */
@Service
public class UserService implements IUserService {

}
