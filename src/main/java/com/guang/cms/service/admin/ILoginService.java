package com.guang.cms.service.admin;

import com.guang.cms.domain.dto.admin.login.LoginResDto;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author lrg
 * @version 1.0
 * @description:
 * @date 2023-07-05  09:34
 */
public interface ILoginService {
    LoginResDto login(String username, String password);

    void logout(HttpServletRequest request);
}
