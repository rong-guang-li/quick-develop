package com.guang.cms.service.admin.impl;


import com.guang.cms.service.admin.IRoleMenuService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色-菜单中间表 服务实现类
 * </p>
 *
 * @author lrg
 * @since 2023-04-21 02:57:54
 */
@Service
public class RoleMenuService implements IRoleMenuService {

}
