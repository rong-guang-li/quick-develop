package com.guang.cms.service.admin.impl;

import com.guang.cms.domain.dto.admin.menu.MenuSaveDto;
import com.guang.cms.service.admin.IRoleService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author lrg
 * @since 2023-04-21 02:57:54
 */
@Service
public class RoleService  implements IRoleService {

    /*
     *业务需求：
     *一个用户只能对应一种角色
     *实际工作中一个用户可能有多种角色类型
     *
     **/

    /**
     * @Author lrg
     * @Description 保存用户角色类型
     * @Date 17:42 2023/4/25
     * @Param []
     * @return void
     **/
    @Override
    public void save(MenuSaveDto menuSaveDto) {
        if(menuSaveDto.getId()==null){
            saveUserRole(menuSaveDto);
        }else {
            updateUserRole(menuSaveDto);
        }
    }

    /**
     * @Author lrg
     * @Description 删除用户角色类型
     * @Date 17:42 2023/4/25
     * @Param [id]
     * @return void
     **/
    @Override
    public void delete(Long id) {

    }

    /**
     * @Author lrg
     * @Description 用户角色类型列表
     * @Date 17:43 2023/4/25
     * @Param []
     * @return void
     **/
    @Override
    public void list() {

    }

    /**
     * @Author lrg
     * @Description 更新用户角色
     * @Date 17:48 2023/4/25
     * @Param [menuSaveDto]
     * @return void
     **/
    private void updateUserRole(MenuSaveDto menuSaveDto) {
    }

    /**
     * @Author lrg
     * @Description 保存用户角色
     * @Date 17:48 2023/4/25
     * @Param [menuSaveDto]
     * @return void
     **/
    private void saveUserRole(MenuSaveDto menuSaveDto) {
    }
}
